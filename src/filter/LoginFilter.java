package filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

import dao.ValidateDao;

public class LoginFilter implements Filter {
	
	/*
	 * 验证登录过滤器
	 * 
	 */
	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request = (HttpServletRequest) req;
		// HttpServletResponse response = (HttpServletResponse) res;
		HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper((HttpServletResponse) res);
		
		String uri = request.getRequestURI();
		
		if (uri.contains("static") || uri.endsWith("login.jsp") || uri.endsWith("login")||uri.endsWith("register")) {
			chain.doFilter(req, res);
			return;
		}else{
			String username = (String)request.getSession().getAttribute("loginFlg");
			if(null==username){
				wrapper.sendRedirect("./login.jsp");
				
				return;
			}
			chain.doFilter(req, res);
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
