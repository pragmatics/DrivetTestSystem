package entity;

import java.util.Date;
import java.util.List;

public class PaperInfo {
	private String pId;
	private Date createTime;
	private String userName;
	private int score;
	private int subject;
	List<QuestionInfo> questions ;
	public String getpId() {
		return pId;
	}
	public void setpId(String pId) {
		this.pId = pId;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public List<QuestionInfo> getQuestions() {
		return questions;
	}
	public void setQuestions(List<QuestionInfo> questions) {
		this.questions = questions;
	}
	public int getSubject() {
		return subject;
	}
	public void setSubject(int subject) {
		this.subject = subject;
	}
	
	
}
