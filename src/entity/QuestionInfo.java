package entity;

import java.util.List;
/*
 * 题目信息实体类
 */
public class QuestionInfo {
	private int qId;
	private String questionContent = "";
	private String imgUrl = "";
	private String explain = "";
	private String label = "";	//之后用于根据题目内容分类
	private int qtype;	//“1”表示单选，“2”表示多选
	private int qsubject;//“1”表示科目一，“4”表示科目四
	private int isDelete;
	
	List<OptionInfo> options;
	
	public int getqId() {
		return qId;
	}
	public void setqId(int qId) {
		this.qId = qId;
	}
	public String getQuestionContent() {
		return questionContent;
	}
	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}
	public String getImgUrl() {
		return imgUrl;
	}
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}
	public String getExplain() {
		return explain;
	}
	public void setExplain(String explain) {
		this.explain = explain;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getQtype() {
		return qtype;
	}
	public void setQtype(int qtype) {
		this.qtype = qtype;
	}
	public int getQsubject() {
		return qsubject;
	}
	public void setQsubject(int qsubject) {
		this.qsubject = qsubject;
	}
	
	public int getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(int isDelete) {
		this.isDelete = isDelete;
	}
	public List<OptionInfo> getOptions() {
		return options;
	}
	public void setOptions(List<OptionInfo> options) {
		this.options = options;
	}
	
	
	
}