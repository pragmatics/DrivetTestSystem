package entity;

public class OptionInfo {
	private int oId;
	private int qId;
	private String optionNum = "";
	private String optionContent = "";
	private int isAnswer;
	
	public int getoId() {
		return oId;
	}
	public void setoId(int oId) {
		this.oId = oId;
	}
	
	
	public int getqId() {
		return qId;
	}
	public void setqId(int qId) {
		this.qId = qId;
	}
	public String getOptionNum() {
		return optionNum;
	}
	public void setOptionNum(String optionNum) {
		this.optionNum = optionNum;
	}
	public String getOptionContent() {
		return optionContent;
	}
	public void setOptionContent(String optionContent) {
		this.optionContent = optionContent;
	}
	public int getIsAnswer() {
		return isAnswer;
	}
	public void setIsAnswer(int isAnswer) {
		this.isAnswer = isAnswer;
	}
	
	
}
