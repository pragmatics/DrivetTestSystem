package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.ValidateDao;

public class LoginServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");

		String userName = request.getParameter("loginUserName");
		String pwd = request.getParameter("loginPassword");
		if (ValidateDao.LoginValidate(userName, pwd).getUserName().equals(userName)) {
			request.getSession().setAttribute("loginFlg", userName);
			if (userName.contains("admin")) {
				response.sendRedirect("./dealData");
			} else {
				response.sendRedirect("./index");
			}
		} else {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		}

	}

}
