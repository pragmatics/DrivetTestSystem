package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import entity.UserInfo;

public class RegisterServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
	throws IOException,ServletException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		UserInfo user = new UserInfo();
		String userName = request.getParameter("registerUserName");
		user.setUserName(request.getParameter("registerUserName"));
		user.setPwd(request.getParameter("registerPassword"));
		
		new UserDao().add(user);
		if(userName.contains("admin")){
			new UserDao().updateAdmin(userName);
		}
		response.sendRedirect("login.jsp");
		
		
	}

}
