package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PaperDao;
import dao.QuestionDao;
import dao.UserDao;
import entity.OptionInfo;
import entity.QuestionInfo;
import util.GetTimeToStr;

/*
 * 专项测试对应的Servlet
 */
public class SpecialTrainServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
			throws IOException,ServletException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		
		String userName = (String)request.getSession().getAttribute("loginFlg");
		int userId = new UserDao().search(userName).getUserId();
		int questionType = Integer.parseInt(request.getParameter("qtype"));
		String paperId = new GetTimeToStr().transDateToPid();
		String createDate = new GetTimeToStr().GetTimeFormat();
		int subject = Integer.parseInt(request.getParameter("subject"));
		
		//添加考试记录
		new PaperDao().addRecord(paperId, userId, createDate, subject);
		//抽题
		List<QuestionInfo> questions = new QuestionDao().extractQusByTyp(questionType, paperId,subject);
		
		request.setAttribute("paperId", paperId);
		request.setAttribute("questions", questions);
		request.getRequestDispatcher("specialTrain.jsp").forward(request,response);
	}

}
