package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PaperDao;
import entity.PaperInfo;

public class SearchRecordServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
			throws IOException,ServletException{
				request.setCharacterEncoding("UTF-8");
				response.setContentType("text/html; charset=UTF-8");
				
				String paperId = request.getParameter("searchValue");
				List<PaperInfo> papers = new PaperDao().queryRecord(paperId);
				request.setAttribute("papers", papers);
				request.getRequestDispatcher("index.jsp").forward(request,response);
				
	}
}
