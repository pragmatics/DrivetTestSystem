package servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PaperDao;
import dao.UserDao;

public class UpdateScoreServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
			throws IOException,ServletException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String paperId = request.getParameter("paperId");
		int score = Integer.parseInt(request.getParameter("score"));
		new PaperDao().updateScore(score, paperId);
		
		
		response.sendRedirect("./index");
	}
}
