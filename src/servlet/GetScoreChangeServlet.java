package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import dao.PaperDao;
import dao.UserDao;
import entity.PaperInfo;


public class GetScoreChangeServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		int subject = Integer.parseInt(request.getParameter("subject"));
		String userName = (String)request.getSession().getAttribute("loginFlg");
		int userId = new UserDao().search(userName).getUserId();
		
		ArrayList<Integer> scoresArr = (ArrayList<Integer>)new PaperDao().listRecordNearFive(userId, subject);
		
		JSONArray json=JSONArray.fromObject(scoresArr);
		//System.out.println(json.toString());
		
		request.setAttribute("jsonScore", json);
		request.getRequestDispatcher("scoreLine.jsp").forward(request,response);
		
	}

}
