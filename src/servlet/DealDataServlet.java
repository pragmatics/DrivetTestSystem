package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;

import dao.QuestionDao;

/*
 * 将数据转换为JSON格式传给ECharts
 */
public class DealDataServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
			throws IOException,ServletException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		
		Map<String,String> map1 = new HashMap<String,String>();
		int value1 = new QuestionDao().getCountByQtype(1);
		map1.put("value", ""+value1);
		map1.put("name", "单选题");
		Map<String,String> map2 = new HashMap<String,String>();
		int value2 = new QuestionDao().getCountByQtype(2);
		map2.put("value", ""+value2);
		map2.put("name", "多选题");
		Map<String,String> map3 = new HashMap<String,String>();
		int value3 = new QuestionDao().getCountByQtype(3);
		map3.put("value", ""+value3);
		map3.put("name", "判断题");		
		String str1 = JSON.toJSONString(map1);
		System.out.println(str1);
		String str2 = JSON.toJSONString(map2);
		String str3 = JSON.toJSONString(map3);
		String jsondata1 = "["+str1+","+str2+","+str3+"]";
		System.out.println(jsondata1);
		
		Map<String,String> map4 = new HashMap<String,String>();
		int value4 = new QuestionDao().getCountByLabel("交规");
		map4.put("value",""+value4);
		map4.put("name", "交规题");
		String str4 = JSON.toJSONString(map4);
		Map<String,String> map5 = new HashMap<String,String>();
		int value5 = new QuestionDao().getCountByLabel("装置");
		map5.put("value",""+value4);
		map5.put("name", "装置题");
		String str5 = JSON.toJSONString(map5);
		Map<String,String> map6 = new HashMap<String,String>();
		int value6 = new QuestionDao().getCountByLabel("路况");
		map6.put("value",""+value6);
		map6.put("name", "路况题");
		String str6 = JSON.toJSONString(map6);
		Map<String,String> map7 = new HashMap<String,String>();
		int value7 = new QuestionDao().getCountByLabel("速度");
		map7.put("value",""+value7);
		map7.put("name", "速度题");
		String str7 = JSON.toJSONString(map7);
		Map<String,String> map8 = new HashMap<String,String>();
		int value8 = new QuestionDao().getCountByLabel("时间");
		map8.put("value",""+value8);
		map8.put("name", "时间题");
		String str8= JSON.toJSONString(map8);
		String jsondata2 = "["+str4+","+str5+","+str6+","+str7+","+str8+"]";
		
		request.setAttribute("jsondata1", jsondata1);
		request.setAttribute("jsondata2", jsondata2);
		request.getRequestDispatcher("admin.jsp").forward(request, response);
	}	
}
