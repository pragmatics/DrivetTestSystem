package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PaperDao;
import dao.QuestionDao;
import dao.UserDao;
import entity.QuestionInfo;
import util.GetTimeToStr;

public class LabelTrainServlet extends HttpServlet {
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		String userName = (String)request.getSession().getAttribute("loginFlg");
		int userId = new UserDao().search(userName).getUserId();
		String label = request.getParameter("label");
		String paperId = new GetTimeToStr().transDateToPid();
		int subject = Integer.parseInt(request.getParameter("subject"));
		String createDate = new GetTimeToStr().GetTimeFormat();
		
		new PaperDao().addRecord(paperId, userId, createDate, subject);
		List<QuestionInfo> questions = new QuestionDao().extractQusByLab(label, paperId, subject);
		
		request.setAttribute("paperId", paperId);
		request.setAttribute("questions", questions);
		request.getRequestDispatcher("specialTrain.jsp").forward(request,response);
		
	}
}
