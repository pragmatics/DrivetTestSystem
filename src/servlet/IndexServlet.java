package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PaperDao;
import dao.UserDao;
import entity.PaperInfo;

public class IndexServlet extends HttpServlet {
	protected void service(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html; charset=UTF-8");
		
		int start = 0;
		int count = 8;
		
		try {
			start = Integer.parseInt(request.getParameter("start"));
		} catch (NumberFormatException e) {
		
		}
		
		int next = start + count;
		int pre = start - count;
		int total = new PaperDao().getTotal();
		
		int last;
		if(0 == total%count)
			last = total -  count;
		else
			last = total - total % count;
		pre = pre < 0 ? 0 : pre;
		next = next > last ? last : next;
		
		request.setAttribute("next", next);
		request.setAttribute("pre", pre);
		request.setAttribute("last", last);
		
		String nowUserName = (String)request.getSession().getAttribute("loginFlg");
		int nowUserId = new UserDao().search(nowUserName).getUserId();
		List<PaperInfo> papers = new PaperDao().listRecord(start,count,nowUserId);
		
		request.setAttribute("papers", papers);
		request.getRequestDispatcher("index.jsp").forward(request,response);
		
	}
}
