package util;

import java.text.SimpleDateFormat;
import java.util.Date;
/*
 * 用于获取当前时间并以一定格式转换成字符串
 */
public class GetTimeToStr {
	//将当前时间转换为字符串，作为考试号
	public String transDateToPid(){
		Date d = new Date();
		long l = d.getTime();
		String str=String.valueOf(l);
		return str;
	}
	//将当前时间转为"yyyy-MM-dd HH:mm:ss"格式的字符串
	public String GetTimeFormat(){
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String str = df.format(new Date());
		return str;
	}
}
