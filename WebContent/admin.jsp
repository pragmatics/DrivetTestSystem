<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="static/bootstrap/css/3.3.6/bootstrap.min.css">
<script src="static/jquery/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="static/bootstrap/js/3.3.6/bootstrap.min.js"
	type="text/javascript"></script>
<script src="static/echarts/echarts.min.js"></script>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
	<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#example-navbar-collapse">
				<span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./dealData">后台管理平台</a>
		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="./questionManage">试题管理</a></li>
				<li><a href="#nowhere">用户管理</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> 其他 <b class="caret"></b>
				</a>
					<ul class="dropdown-menu">
						<li><a href="#">回收站</a></li>
						<li><a href="#">联系我们</a></li>
						<li><a href="login.jsp">退出账号</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	</nav>
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-6 column">
				<div id="QusPie1" style="width: 500px; height: 500px;"></div>
			</div>
			<div class="col-md-6 column">
				<div id="QusPie2" style="width: 500px; height: 500px;"></div>
			</div>
		</div>
	</div>


	<script type="text/javascript">
		//初始化Echarts	
		var myChart1 = echarts.init(document.getElementById('QusPie1'));
		var myChart2 = echarts.init(document.getElementById('QusPie2'));
		var jsondata1 =
	<%=request.getAttribute("jsondata1")%>
		;
		var jsondata2 =
	<%=request.getAttribute("jsondata2")%>
		;

		//var jsondata = eval('(' + data + ')');
		//alert(typeOf(jsondata));

		// 指定图表的配置项和数据  
		var option1 = {
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b}: {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				x : 'left',
				data : [ '单选题', '多选题', '判断题' ]
			},
			series : [ {
				name : '当前题数',
				type : 'pie',
				radius : [ '50%', '70%' ],
				avoidLabelOverlap : false,
				label : {
					normal : {
						show : false,
						position : 'center'
					},
					emphasis : {
						show : true,
						textStyle : {
							fontSize : '30',
							fontWeight : 'bold'
						}
					}
				},
				labelLine : {
					normal : {
						show : false
					}
				},
				data : jsondata1,

			} ]
		};
		var option2 = {
			tooltip : {
				trigger : 'item',
				formatter : "{a} <br/>{b}: {c} ({d}%)"
			},
			legend : {
				orient : 'vertical',
				x : 'left',
				data : [ '交规题', '装置题', '路况题', '时间题', '速度题' ]
			},
			series : [ {
				name : '当前题数',
				type : 'pie',
				radius : [ '50%', '70%' ],
				avoidLabelOverlap : false,
				label : {
					normal : {
						show : false,
						position : 'center'
					},
					emphasis : {
						show : true,
						textStyle : {
							fontSize : '30',
							fontWeight : 'bold'
						}
					}
				},
				labelLine : {
					normal : {
						show : false
					}
				},
				data : jsondata2,

			} ]
		};

		// 使用刚指定的配置项和数据显示图表。
		myChart1.setOption(option1);
		myChart2.setOption(option2);
	</script>
</body>
</html>