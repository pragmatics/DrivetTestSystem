<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="static/bootstrap/css/3.3.6/bootstrap.min.css">
<script src="static/jquery/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="static/bootstrap/js/3.3.6/bootstrap.min.js"
	type="text/javascript"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<style>
* {
	box-sizing: border-box;
}

body {
	margin: 0;
	background-color: #f3f3f4;
}

.header {
	background-color: #2196F3;
	color: white;
	text-align: center;
	padding: 15px;
}

.topmenu {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	background-color: #777;
}

.topmenu li {
	float: left;
}

.topmenu li a {
	display: inline-block;
	color: white;
	text-align: center;
	padding: 16px;
	text-decoration: none;
}

.topmenu li a:hover {
	background-color: #222;
}

.topmenu li a.active {
	color: white;
	background-color: #4CAF50;
}

.column {
	float: left;
	padding: 15px;
}

.clearfix::after {
	content: "";
	clear: both;
	display: table;
}

.sidemenu {
	width: 15%;
	text-align: center;
}

.content {
	width: 75%;
}

.sidemenu ul {
	list-style-type: none;
	margin: 0;
	padding: 0;
}

.sidemenu li a {
	margin-bottom: 4px;
	display: block;
	padding: 8px;
	background-color: #fff;
	text-decoration: none;
	color: #777;
}

.sidemenu li a:hover {
	background-color: #555;
	color: white;
}

.sidemenu li a.active {
	color: white;
}

ul.double-ul {
	display: none;
}

li.thr-li a:hover {
	background-color: #008CBA;
}
thead{
	background-color:#1cc09f;
	color:white;
	font-weight: bold;
	font-size:16px;
}
.searchDiv{
	margin:20px;
}
.record table{
	font-family:"open sans","Helvetica Neue",Helvetica,Arial,sans-serif;
	font-size:15px;
}
td{
	border:1px solid #e7e7e7;
}
.existDiv {
	float: right;
}

.existDiv li a:hover {
	background-color: rgb(217, 116, 43);
	color: white;
}
</style>
<script>
$(document).ready(function(){
    $("tbody").find("tr").bind("mouseover",function(){  
        $(this).css("background-color","#eeeeee");  
    });  
    $("tbody").find("tr").bind("mouseout",function(){  
        $(this).css("background-color","#ffffff");  
    });

});
	function toggleDoubleUl(uid) {
		$("ul.double-ul[uid=" + uid + "]").toggle(500);
	}
	$(function() {
		$("li.fir-ul").click(function() {
			var uid = $(this).attr("uid");
			toggleDoubleUl(uid);
		});

		$("li.double-ul").click(function() {
			var uid = $(this).attr("uid");
			toggleDoubleUl(uid);
		});

	});
</script>
</head>
<body>

	<ul class="topmenu">
		<li><a href="./index" class="active">主页</a></li>
		<li><a href="knowledgeStudy.jsp">理论学习</a></li>
		<li><a href="./indexBySubject?subject=1">科目一</a></li>
		<li><a href="./indexBySubject?subject=4">科目四</a></li>
		<div class="existDiv">
			<li><a href="login.jsp">退出账号</a></li>
		</div>
	</ul>

	<div class="clearfix">
		<div class="column sidemenu">
			<ul>
				<li class="fir-ul" uid="1"><a href="#specialTrain">专项练习</a>
					<ul class="double-ul" uid="1">
						<li class="thr-li"><a href="./trainByType?qtype=1&subject=${subject}"
							>单选题</a></li>
						<li class="thr-li"><a href="./trainByType?qtype=3&subject=${subject}"
							>判断题</a></li>
						<li class="thr-li"><a href="./trainByLabel?label=图片&subject=${subject}"
							>图片题</a></li>
						<li class="thr-li"><a href="./trainByLabel?label=装置&subject=${subject}"
							>装置题</a></li>
						<li class="thr-li"><a href="./trainByLabel?label=速度&subject=${subject}"
							>速度题</a></li>
						<li class="thr-li"><a href="./trainByLabel?label=时间&subject=${subject}"
							>时间题</a></li>		
					</ul></li>
				<li class="fir-ul" uid="2"><a href="./simulationTest?subject=${subject}">模拟考试</a>
					</li>
			</ul>
		</div>

		<div class="column content">
			<div class="header">
				<h1>科目${subject}</h1>
			</div>
			<div class = searchDiv>
			<form action="searchRecord" method="post" id="listRecord">
				<div class="input-group">
					<input type="text" name="searchValue" class="form-control"
						placeholder="请输入考试号"> <span class="input-group-btn">
						<button type="submit" class="btn btn-success" form="listRecord"
							value="Submit">搜索</button>
					</span>
				</div>
			</form>
			</div>
			<div class="record table">
				<table id="listTable" class="table table-condensed" border='1'
					cellspacing='0' id="HistoryTable"
					style="border-radius: 5px; overflow: hidden;">
					<thead>
						<tr>
							<td>考试编号</td>
							<td>考试时间</td>
							<td>用户昵称</td>
							<td>考试得分</td>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${papers}" var="paper" varStatus="st">
							<tr>
								<td>${paper.getpId()}</td>
								<td>${paper.getCreateTime()}</td>
								<td>${paper.getUserName()}</td>
								<td>${paper.getScore()}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div align="center">
					<ul class="pagination">
						<li><a href="?start=0&subject=${subject}">首 页</a></li>
						<li><a href="?start=${pre}&subject=${subject}">&laquo;上一页</a></li>
						<li><a href="?start=${next}&subject=${subject}">下一页&raquo;</a></li>
						<li><a href="?start=${last}&subject=${subject}">末 页</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</body>
</html>