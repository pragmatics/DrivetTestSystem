<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="static/bootstrap/css/3.3.6/bootstrap.min.css">
<script src="static/jquery/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="static/bootstrap/js/3.3.6/bootstrap.min.js"
	type="text/javascript"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<style>
thead {
	background-color: rgb(96, 143, 159);
	color: white;
	font-weight: bold;
	font-size: 16px;
}
</style>
<body>
	<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#example-navbar-collapse">
				<span class="sr-only">切换导航</span> <span class="icon-bar"></span> <span
					class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="./dealData">后台管理平台</a>
		</div>
		<div class="collapse navbar-collapse" id="example-navbar-collapse">
			<ul class="nav navbar-nav">
				<li class="active"><a href="./questionManage">试题管理</a></li>
				<li><a href="#nowhere">用户管理</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown"> 其他 <b class="caret"></b>
				</a>
					<ul class="dropdown-menu">
						<li><a href="#">回收站</a></li>
						<li><a href="#">联系我们</a></li>
						<li><a href="login.jsp">退出账号</a></li>
					</ul></li>
			</ul>
		</div>
	</div>
	</nav>
	<div class="record table">
		<table id="listTable" class="table table-condensed" border='1'
			cellspacing='0' id="HistoryTable"
			style="border-radius: 5px; overflow: hidden;">
			<thead>
				<tr>
					<td>题目编号</td>
					<td>题目内容</td>
					<td>删除</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${questions}" var="question" varStatus="st">
					<tr>
						<td>${question.getqId()}</td>
						<td><a href="./qusDetail?qId=${question.getqId()}">
								${question.getQuestionContent()} </a></td>
						<td><a href="./deleteQus?qId=${question.getqId()}"
							onclick="deleteRow(this)">点此删除</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<div align="center">
			<ul class="pagination">
				<li><a href="?start=0">首 页</a></li>
				<li><a href="?start=${pre}">&laquo;上一页</a></li>
				<li><a href="?start=${next}">下一页&raquo;</a></li>
				<li><a href="?start=${last}">末 页</a></li>
			</ul>

			<div style="text-align: center; height: 50px; line-height: 50px;">
				<button class="btn btn-info btn-lg" data-toggle="modal"
					data-target="#myModal">添加试题</button>
			</div>
			<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
				aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal"
								aria-hidden="true">&times;</button>
							<h4 class="modal-title" id="myModalLabel">添加试题</h4>
						</div>

						<!-- 表格 -->
						<div class="modal-body">
							<form id='addForm' class="form-horizontal" action="addNewQus"
								method="post" enctype="multipart/form-data">
								<div class="col-md-12">
									<div class="form-group">
										<label class="col-sm-3 control-label">题目内容</label>
										<div class="col-sm-9">
											<input type="text" name="questionContent"
												class="form-control" placeholder="请输入文本">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">本题解析</label>
										<div class="col-sm-9">
											<input type="text" name="explain" class="form-control"
												placeholder="请输入文本">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">科目</label>
										<div class="col-sm-9">
											<label class="radio-inline"> <input type="radio"
												value="1" class="optionsRadios" name="qsubject">科目一
											</label> <label class="radio-inline"> <input type="radio"
												value="4" class="optionsRadios" name="qsubject">科目四
											</label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">题目类型</label>
										<div class="col-sm-9">
											<label class="checkbox-inline"> <input type="radio"
												value="1" name="qtype" class="qtypeRadio">单选题
											</label> <label class="checkbox-inline"> <input type="radio"
												value="2" name="qtype" class="qtypeRadio">多选题
											</label> <label class="checkbox-inline"> <input type="radio"
												value="3" name="qtype" class="qtypeRadio">判断题
											</label>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label">题目分类</label>
										<div class="col-sm-9">
											<label class="checkbox-inline"> <input
												type="checkbox" class = "picChk" value="图片题" name="label">图片题
											</label> <label class="checkbox-inline"> <input
												type="checkbox" value="时间题" name="label">时间题
											</label> <label class="checkbox-inline"> <input
												type="checkbox" value="交规题" name="label">交规题
											</label> <label class="checkbox-inline"> <input
												type="checkbox" value="装置题" name="label">装置题
											</label> <label class="checkbox-inline"> <input
												type="checkbox" value="速度题" name="label">速度题
											</label>
										</div>
									</div>
									<div class="form-group" id="uploadDiv" style="display: none">
										<label class="col-sm-3 control-label">图片上传：</label>
										<div class="col-sm-9">
											<input type="file" name="filepath" class="form-control">
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"> <input
											type="radio" value="A" name="correctAnswer"
											class="correctAnswer"> &nbsp;&nbsp;选项A
										</label>

										<div class="col-sm-9">
											<input type="text" name="optionA" class="form-control"
												placeholder="请输入文本">

										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-3 control-label"> <input
											type="radio" value="B" name="correctAnswer"
											class="correctAnswer"> &nbsp;&nbsp;选项B
										</label>
										<div class="col-sm-9">
											<input type="text" name="optionB" class="form-control"
												placeholder="请输入文本">

										</div>
									</div>
									<div class="form-group" id="optionCDiv">
										<label class="col-sm-3 control-label"> <input
											type="radio" value="C" name="correctAnswer"
											class="correctAnswer"> &nbsp;&nbsp;选项C
										</label>
										<div class="col-sm-9">
											<input type="text" name="optionC" class="form-control"
												placeholder="请输入文本">
										</div>
									</div>
									<div class="form-group" id="optionDDiv">
										<label class="col-sm-3 control-label"> <input
											type="radio" value="D" name="correctAnswer"
											class="correctAnswer"> &nbsp;&nbsp;选项D
										</label>
										<div class="col-sm-9">
											<input type="text" name="optionD" class="form-control"
												placeholder="请输入文本">

										</div>
									</div>
									<span class="help-block m-b-none">不要忘记勾选正确选项哦！</span>
								</div>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default"
								data-dismiss="modal">关闭</button>
							<button type="submit" form="addForm" class="btn btn-primary" onclick = "validatePic()">提交</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal -->
			</div>
		</div>

	</div>
</body>
<script>
	function deleteRow(link) {
		var b = confirm("确定删除？")
		if (!b)
			return false;

		var table = document.getElementById("listTable");
		var td = link.parentNode;
		var tr = td.parentNode;
		var index = tr.rowIndex;
		table.deleteRow(index);
	};
	//提交表单时如果没上传图片，删除上传图片的控件元素
	function validatePic(){
		if(!($(".picChk").is(":checked"))){
			$("#uploadDiv").remove();
		}
	};
	$(document).ready(function() {
		    $("tbody").find("tr").bind("mouseover",function(){  
		        $(this).css("background-color","#eeeeee");  
		    });  
		    $("tbody").find("tr").bind("mouseout",function(){  
		        $(this).css("background-color","#ffffff");  
		    });

		  //暂时无法生效，原因可能是bootstrap.css已经默认了悬停的颜色
		    $(".dropdown-menu").find("li").bind("mouseover",function(){  
		    $(this).css("background-color","rgb(96,143,159)");  
		    });  
		    $(".dropdown-menu").find("li").bind("mouseout",function(){  
		    $(this).css("background-color","#ffffff");  
		    });
	  
		//区分单选，多选和判断的勾选框
		$(".qtypeRadio").click(function() {
			var qtype = $('input:radio[name="qtype"]:checked').val();
			if (qtype == 3) {
				$(".correctAnswer").attr('type', 'radio');
				$("#optionCDiv").hide();
				$("#optionDDiv").hide();
			} else if (qtype == 2) {
				$(".correctAnswer").attr('type', 'checkbox');
				$("#optionCDiv").show();
				$("#optionDDiv").show();
			} else {
				$(".correctAnswer").attr('type', 'radio');
				$("#optionCDiv").show();
				$("#optionDDiv").show();
			}
		});
		//选中题目分类为“图片题”，上传文件控件出现
		$(".picChk").click(function(){
			$("#uploadDiv").toggle(500);
		});
	});
</script>
</html>