<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="static/bootstrap/css/3.3.6/bootstrap.min.css">
<script src="static/jquery/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="static/bootstrap/js/3.3.6/bootstrap.min.js"
	type="text/javascript"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<style>
body {
	background-color: #fbfbfb;
}

.questionDiv {
	border: solid 2px rgb(0,49,79);
}

.questionContentDiv {
	color: rgb(255,255,255);
}

.optionDiv {
	color: darkgrey;
}

.questionContentDiv {
	background-color: darkgrey;
	background-color: #96b97d;
	padding-top: 20px;
	width: 1290px;
	height: 80px;
	font-weight: bold;
	font-size: 16px;
}

.explainStr {
	color: rgb(160, 191, 124);
	font-weight: bold;
	float: right;
}

.explainDisplay {
	color: rgb(3,35,14);
}

.submitPaper {
	margin-top: 30px;
	margin-left: 650px;
	background-color: rgb(64,116,52);
	border: none;
	color: white;
	padding: 10px 20px;
	text-align: center;
	text-decoration: none;
	display: inline-block;
	font-size: 16px;
	border-radius: 5px;
}
.demo--label{
	margin:20px 20px 0 0;
	display:inline-block
}
.demo--radioInput{
	background-color:#fff;
	border:1px solid rgba(0,0,0,0.15);
	border-radius:100%;
	display:inline-block;
	height:16px;margin-right:10px;
	margin-top:-1px;
	vertical-align:middle;
	width:16px;
	line-height:1
}
.demo--radio{
	display:none
}
.demo--radio:checked + .demo--radioInput:after{
	background-color:#57ad68;
	border-radius:100%;
	content:"";
	display:inline-block;
	height:10px;
	margin:2px;
	width:10px
}
.demo--checkbox.demo--radioInput,.demo--radio:checked + .demo--checkbox.demo--radioInput:after{
	border-radius:0
}

</style>
<body>
	<h3 style="font-size: 250%; text-align: center">考试编号&nbsp;${paperId}</h3>
	<c:forEach items="${questions}" var="question" varStatus="s">
		<div class="questionDiv">
			<div class="questionContentDiv">${s.index+1}&nbsp;&nbsp;${question.getQuestionContent()}</div>
			<div class="explain">
				<div class="explainStr" name="explainStr_${s.index}">本题分析</div>
				<div class="explainDisplay" name="expDiv_${s.index}"
					style="display: none">${question.getExplain()}</div>
			</div>
			<br> <div class = "questionImgDiv">
				<img src="${question.getImgUrl()}">
			</div> 
			<div class="optionDiv">
				<c:choose>
					<c:when test="${question.getQtype() == '1'}">
						<c:forEach items="${question.getOptions()}" var="option"
							varStatus="st">
							 <label class="demo--label">
							<input class="demo--radio" type="radio" name="radio_${s.index}"
								value="${option.getIsAnswer()}">
								<span class="demo--radioInput"></span>
								${option.getOptionContent()}
								</label>
								<br>
						</c:forEach>
					</c:when>
					<c:when test="${question.getQtype() == '3'}">
						<c:forEach items="${question.getOptions()}" var="option"
							varStatus="st">
							<label class="demo--label">
							<input class="demo--radio" type="radio" name="radio_${s.index}"
								value="${option.getIsAnswer()}">
								<span class="demo--radioInput"></span>
								${option.getOptionContent()}
								</label>
								<br>
						</c:forEach>
					</c:when>
					<c:otherwise>
						<c:forEach items="${question.getOptions()}" var="option"
							varStatus="st">
							<label class="demo--label">
							<input class="demo--radio" type="checkbox"
								name="chkbox_${s.index}" value="${option.getIsAnswer()}">
								<span class="demo--checkbox demo--radioInput"></span>
								${option.getOptionContent()}
								</label>
								<br>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</div>
			<br>
		</div>
	</c:forEach>
	<input type="button" class="submitPaper" onclick="getScore()"
		value="提交试卷">
</body>
<script type="text/javascript">
	//计算得分
	function getScore() {
		var score = 0;
		var type = $("input").attr("type");
		if (type == "radio") {
			for (var i = 0; i < 10; i++) {
				//获取第i题用户所选的单选框的value
				var checked = $("input:radio[name='radio_" + i + "']:checked")
						.val();
				if (checked == 1) {
					score = score + 10;
				}
			}
		} else if (type == "checkbox") {
			for (var i = 0; i < 10; i++) {
				//分别获取第i题已选checkbox和未选的checkbox
				var uncheckedList = new Array();
				$("input[name='chkbox_" + i + "']").not("input:checked").each(
						function() {
							uncheckedList.push($(this).val());
						});
				var checkedList = new Array();
				$("input[name='chkbox_" + i + "']:checked").each(function() {
					checkedList.push($(this).val());
				});
				//如果已选数组里value全是1并且未选数组里全是0，则做对此题
				if ((uncheckedList.indexOf("1") == -1)
						|| (checkedList.indexOf("0")) == -1) {
					score = score + 10;
				}
			}
		}
		alert("考试结束，您的成绩是" + score + "分！");
		window.location.href = "./finish?score=" + score + "&paperId=" + ${paperId};
	}
	//查看题目解析
	$(function() {
		$(".explainStr").click(function() {
			$(this).next().toggle();
		})
	})
	//隐藏非图片题的图片区域
	$(function(){
		$("img").each(function(){
			if($(this).attr("src").length == 0){
				$(this).parent().css('display','none');
			}
		})
	})
</script>
</html>