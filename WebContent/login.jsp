<%@page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	import="java.util.*"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="static/jquery/jquery-2.2.4.min.js" type="text/javascript"></script>
<link rel="stylesheet"
	href="static/bootstrap/css/3.3.6/bootstrap.min.css">
<script src="static/bootstrap/js/3.3.6/bootstrap.min.js"
	type="text/javascript"></script>
</head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script>
	$(document).ready(function() {

		$(".form").slideDown(500);

		$("#login").addClass("border-btn");

		$("#register").click(function() {
			$("#login").removeClass("border-btn");
			$("#login-content").hide(500);
			$(this).addClass("border-btn");
			$("#register-content").show(500);

		})

		$("#login").click(function() {
			$("#register").removeClass("border-btn");
			$(this).addClass("border-btn");

			$("#login-content").show(500);
			$("#register-content").hide(500);
		})
		
	});
</script>
<style>
* {
	margin: 0;
	padding: 0;
	font-family: "微软雅黑";
}

body {
	background-image:url(./static/img/background.jpg) ;
}

.form {
	position: absolute;
	top: 50%;
	left: 50%;
	margin-left: -185px;
	margin-top: -210px;
	height: 420px;
	width: 340px;
	font-size: 18px;
	-webkit-box-shadow: 0px 0px 10px #A6A6A6;
	background: #fff;
}

.inp {
	height: 30px;
	margin: 0 auto;
	margin-bottom: 30px;
	width: 200px;
}

.inp>input {
	text-align: center;
	height: 30px;
	width: 200px;
	margin: 0 auto;
	transition: all 0.3s ease-in-out;
}

.border-btn {
	border-bottom: 1px solid #ccc;
}

#login, #register {
	float: left;
	text-align: center;
	width: 170px;
	padding: 15px 0;
	color: #545454;
	width: 170px;
	padding: 15px 0;
}

#login-content {
	clear: both;
}

#photo {
	border-radius: 80px;
	border: 1px solid #ccc;
	height: 80px;
	width: 80px;
	margin: 0 auto;
	overflow: hidden;
	clear: both;
	margin-top: 30px;
	margin-bottom: 30px;
}

#photo>img:hover {
	-webkit-transform: rotateZ(360deg);
	-moz-transform: rotateZ(360deg);
	-o-transform: rotateZ(360deg);
	-ms-transform: rotateZ(360deg);
	transform: rotateZ(360deg);
}

#photo>img {
	height: 80px;
	width: 80px;
	-webkit-background-size: 220px 220px;
	border-radius: 60px;
	-webkit-transition: -webkit-transform 1s linear;
	-moz-transition: -moz-transform 1s linear;
	-o-transition: -o-transform 1s linear;
	-ms-transition: -ms-transform 1s linear;
}

#register-content {
	margin-top: 100px;
	display: none;
}

.border-btn {
	border-bottom: 1px solid #ccc;
}
</style>
<body>
	<div class="form">

		<div id="login">登录</div>
		<div id="register">注册</div>
		<div class="content" style="text-align: center">
			<div id="login-content">
				<div id="photo">
					<img id="headImg" src="static/img/heiyiren.jpg" />
				</div>
				<form action="login" method="post">
					<div class="inp">
						<!-- 通过onblur="showHeadImg()"新增显示头像功能 -->
						<input type="text" id="loginUserName" name="loginUserName" placeholder="请输入姓名">
					</div>
					<div class="inp">
						<input type="password" id="loginPassword" name="loginPassword" placeholder="请输入密码">
					</div>
					<input type="submit" value="登录">
				</form>
			</div>
			<div id="register-content">
				<form action="register" onsubmit="return register()">
					<!-- id为自增长，不需要插入 -->
					<div class="inp">
						<input type="text" id="username" name="registerUserName"
							placeholder="请输入用户名">
					</div>
					<div class="inp">
						<input type="text" id="password" name="registerPassword"
							placeholder="请输入密码">
					</div>

					<input type="submit" value="立即注册">
				</form>
			</div>
		</div>

	</div>
	<img src="${imgPath}">
</body>
<script>
	function register() {
		//判断用户名
		var name = document.getElementById("username");
		if (name.value.length < 3) {
			alert("用户名至少需要3位长度");
			return false;
		}
		//判断密码（之后加入至少几位英文几位数字的判断）
		var password = document.getElementById("password");
		if (0 == password.value.length) {
			alert("密码不能为空");
			return false;
		}


	}
	
	
	<!--
	function showHeadImg() {
		var username = $("loginUserName").val;
		$.ajax({
			type : "POST",
			url : "./getHeadImg",
			data : {
				username : "username"
			},
		});
	}
	-->
</script>


