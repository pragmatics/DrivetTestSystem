<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="static/bootstrap/css/3.3.6/bootstrap.min.css">
<script src="static/jquery/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="static/bootstrap/js/3.3.6/bootstrap.min.js"
	type="text/javascript"></script>
	<script src="static/echarts/echarts.min.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
</head>
<body>
<div class="container">
	<div class="row clearfix">
		<div class="col-md-2 column">
		</div>
		<div class="col-md-8 column">
		  <div id="box" style="width: 780px; height:500px; "></div>
		
		</div>
		<div class="col-md-2 column">
		</div>
	</div>
</div>
<script type="text/javascript">
var myChart = echarts.init(document.getElementById('box'));
var jsondata = <%=request.getAttribute("jsonScore")%>;
var option = {
	    xAxis: {
	        type: 'category',
	        data: ['第一次', '第二次', '第三次', '第四次', '第五次', '第六次', '第七次']
	    },
	    yAxis: {
	        type: 'value',
	        min:0,
	        max:100
	    },
	    series: [{
	        data: jsondata,
	        type: 'line',
	        symbol: 'triangle',
	        symbolSize: 20,
	        lineStyle: {
	            normal: {
	                color: 'green',
	                width: 4,
	                type: 'dashed'
	            }
	        },
	        itemStyle: {
	            normal: {
	                borderWidth: 3,
	                borderColor: 'yellow',
	                color: 'blue'
	            }
	        }
	    }]
	};

myChart.setOption(option); 
</script>
</body>
</html>